package com.meiming.smartfloatball;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

      //  Rect rect = new Rect();
        // /取得整个视图部分,注意，如果你要设置标题样式，这个必须出现在标题样式之后，否则会出错
     //   getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
      //  int top = rect.top;//状态栏的高度，所以rect.height,rect.width分别是系统的高度的宽度

      //  Log.i("top",""+top);
        Intent service = new Intent();
        service.setClass(this, TopFloatService.class);
        //启动服务
        startService(service);

        //结束当前界面
        finish();
    }
}
