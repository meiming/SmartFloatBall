package com.meiming.smartfloatball;

import android.app.Application;
import android.view.WindowManager;

/**
 * Created by meiming on 16-1-26.
 */
public class MyApplication extends Application {
    private WindowManager.LayoutParams wmParams = new WindowManager.LayoutParams();
    private WindowManager.LayoutParams pointerParams = new WindowManager.LayoutParams();

    public WindowManager.LayoutParams getMywmParams() {
        return wmParams;
    }

    public WindowManager.LayoutParams getMyPointerParams() {
        return pointerParams;
    }
}
