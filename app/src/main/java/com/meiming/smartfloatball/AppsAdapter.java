package com.meiming.smartfloatball;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by meiming on 16-2-1.
 */
public class AppsAdapter extends BaseAdapter {

    private Context mContext ;
    private PackageManager mPackageManager;
    private List<ResolveInfo> mAllApps ;

    AppsAdapter(Context context , PackageManager packageManager, List<ResolveInfo> allApps) {
        mContext = context ;
        mPackageManager = packageManager;
        mAllApps = allApps ;
    }

    @Override
    public int getCount() {
        return mAllApps.size();
    }

    @Override
    public Object getItem(int position) {
        return mAllApps.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(mContext).inflate(R.layout.applications, null);
        ImageView icon = (ImageView)convertView.findViewById(R.id.icon);
        TextView title = (TextView)convertView.findViewById(R.id.title);
        ResolveInfo res = mAllApps.get(position);
        icon.setImageDrawable(res.loadIcon(mPackageManager));
        title.setText(res.loadLabel(mPackageManager));
        return convertView;
    }
}
