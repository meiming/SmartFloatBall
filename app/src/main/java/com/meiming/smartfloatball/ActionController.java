package com.meiming.smartfloatball;

import android.util.Log;
import android.view.KeyEvent;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.TreeMap;

/**
 * Created by meiming on 16-1-27.
 */
public class ActionController {
    private static final String TAG = ActionController.class.getSimpleName();


    public static void executCommand(final String strCmd){
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    Runtime.getRuntime().exec(strCmd);
                } catch (IOException e) {
                    // TODO handle I/O going wrong
                    // this probably means that the device isn't rooted
                    Log.i(TAG, "handle I/O going wrong");
                }

            }

        }).start();
    }

    public static void executCommandAsRoot(final String strCmd){
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    Process proc = Runtime.getRuntime().exec("su");
                    DataOutputStream os = new DataOutputStream(proc.getOutputStream());

                    os.writeBytes(strCmd+"\n");
                    os.flush();

                    os.writeBytes("exit\n");
                    os.flush();

                    if (proc.waitFor() == 255) {
                        // TODO handle being declined root access
                        // 255 is the standard code for being declined root for SU
                        Log.i(TAG, "handle being declined root access");
                    }
                } catch (IOException e) {
                    // TODO handle I/O going wrong
                    // this probably means that the device isn't rooted
                    Log.i(TAG, "handle I/O going wrong");
                } catch (InterruptedException e) {
                    // don't swallow interruptions
                    Thread.currentThread().interrupt();
                }

            }

        }).start();
    }

    public static void systemKey(int keyCode){
        executCommandAsRoot("input keyevent " + keyCode);
    }

    public static void keyBack(){
        systemKey(KeyEvent.KEYCODE_BACK);
    }
    public static void keyHome(){
        systemKey(KeyEvent.KEYCODE_HOME);
    }
    public static void keyCall(){
        systemKey(KeyEvent.KEYCODE_CALL);
    }
    public static void keyEndCall(){
        systemKey(KeyEvent.KEYCODE_ENDCALL);
    }
    public static void keyCamera(){
        systemKey(KeyEvent.KEYCODE_CAMERA);
    }
    public static void reboot(){
        executCommandAsRoot("reboot");
    }


    public static void click(final int x, final int y){
        new Thread(new Runnable() {
            @Override
            public void run() {
               /* try {
                //    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }*/
                executCommandAsRoot("input tap " + x + " " + y);
            }
        }).start();

    }

    public static void weixinAA(){
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    click(360,1000);
                    Thread.sleep(5000);
                    click(360,800);
                    Thread.sleep(1000);
                    click(360,850);
                    Thread.sleep(100);
                    click(650,950);
                    Thread.sleep(100);
                    click(360,950);
                    Thread.sleep(100);
                    click(130,950);
                    Thread.sleep(100);
                    click(360,1050);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }).start();

    }


    public static void cmdTest(){
        executCommandAsRoot("input tap 150 700");
    }
}
